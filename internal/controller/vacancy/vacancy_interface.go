package vacancy

import "net/http"

type Vancer interface {
    Search(http.ResponseWriter, *http.Request)
    Delete(http.ResponseWriter, *http.Request)
    Get(http.ResponseWriter, *http.Request)
    List(http.ResponseWriter, *http.Request)
}
