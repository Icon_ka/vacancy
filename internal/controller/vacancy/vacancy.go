package vacancy

import (
    "net/http"
    "vacancy/internal/service"
    "context"
    "vacancy/internal/entites"
    "encoding/json"
    "fmt"
    "github.com/jmoiron/sqlx"
)

type VacancyController struct {
    service service.Service
}

func NewVacancyController(db *sqlx.DB) *VacancyController {
    return &VacancyController{service: *service.NewService(db)}
}

// Search godoc
// @Summary     Поиск вакансий.
// @Description Ищет вакансии и записывает их базу данных.
// @Accept      json
// @Produce     json
// @Param       body body entites.Request true "Запрос на поиск вакансий"
// @Success     200 {object} []entites.Vacancy "Успешный ответ"
// @Router      /search [post]
func (v VacancyController) Search(writer http.ResponseWriter, request *http.Request) {
    var req entites.Request
    err := json.NewDecoder(request.Body).Decode(&req)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        writer.Write([]byte("Ошибка при декодировании JSON"))
    }

    res, err := v.service.Vac.Create(context.Background(), req.Query)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        writer.Write([]byte("Ошибка при запросе к базе данных"))
    }

    writer.WriteHeader(http.StatusOK)
    fmt.Fprint(writer, res)
}

// Delete godoc
// @Summary     Удаление вакансии.
// @Description Удаляет вакансию из базы данных.
// @Accept      json
// @Produce     json
// @Param       body body entites.DeleteGetRequest true "Запрос на удаление вакансии"
// @Success     200 "Вакансия успешно удалена"
// @Router      /delete [post]
func (v VacancyController) Delete(writer http.ResponseWriter, request *http.Request) {
    var req entites.DeleteGetRequest
    err := json.NewDecoder(request.Body).Decode(&req)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        writer.Write([]byte("Ошибка при декодировании JSON"))
    }

    err = v.service.Vac.Delete(context.Background(), req.ID)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        writer.Write([]byte("Ошибка при запросе к базе данных"))
    }

    writer.WriteHeader(http.StatusOK)
    fmt.Fprint(writer, "Вакансия удалена")
}

// Get godoc
// @Summary     Ищет вакансию в базе данных.
// @Description Ищет вакансию в базе данных по её ID.
// @Accept      json
// @Produce     json
// @Param       body body entites.DeleteGetRequest true "Запрос на поиск вакансий"
// @Success     200 {object} entites.Vacancy "Успешный ответ"
// @Router      /get [post]
func (v VacancyController) Get(writer http.ResponseWriter, request *http.Request) {
    var req entites.DeleteGetRequest
    err := json.NewDecoder(request.Body).Decode(&req)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        writer.Write([]byte("Ошибка при декодировании JSON"))
    }

    res, err := v.service.Vac.GetByID(context.Background(), req.ID)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        writer.Write([]byte("Ошибка при запросе к базе данных"))
    }

    writer.WriteHeader(http.StatusOK)
    fmt.Fprint(writer, res)
}

// List godoc
// @Summary     Вывод всех вакансий.
// @Description Выводит все вакансии из базы данных.
// @Accept      json
// @Produce     json
// @Success     200 {object} []entites.Vacancy "Успешный ответ"
// @Router      /list [get]
func (v VacancyController) List(writer http.ResponseWriter, request *http.Request) {
    res, err := v.service.Vac.List(context.Background())
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        writer.Write([]byte("Ошибка при запросе к базе данных"))
    }

    writer.WriteHeader(http.StatusOK)
    fmt.Fprint(writer, res)
}
