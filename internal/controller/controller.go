package controller

import (
    "github.com/jmoiron/sqlx"
    "vacancy/internal/controller/vacancy"
)

type Controller struct {
    Ctrl vacancy.Vancer
}

func NewController(db *sqlx.DB) *Controller {
    return &Controller{Ctrl: vacancy.NewVacancyController(db)}
}
