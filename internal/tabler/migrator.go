package tabler

import (
    "golang.org/x/sync/errgroup"
    "github.com/jmoiron/sqlx"
)

type Migratorer interface {
    Migrate(tables ...Tabler) error
}

type Migrator struct {
    Db           *sqlx.DB
    SqlGenerator SQLGenerator
}

func NewMigrator(db *sqlx.DB, sqlGenerator SQLGenerator) *Migrator {
    return &Migrator{
        Db:           db,
        SqlGenerator: sqlGenerator,
    }
}

func (m *Migrator) Migrate(tables ...Tabler) error {
    var errGroup errgroup.Group
    for _, table := range tables {
        createSQL := m.SqlGenerator.CreateTableSQL(table)
        errGroup.Go(func() error {
            _, err := m.Db.Exec(createSQL)
            return err
        })
    }

    return errGroup.Wait()
}
