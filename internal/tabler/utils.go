package tabler

import (
    "reflect"
)

type Tabler interface {
    TableName() string
}

type StructInfo struct {
    Fields []string
}

func GetStructInfo(u interface{}, args ...func(*[]reflect.StructField)) StructInfo {
    val := reflect.ValueOf(u)
    if val.Kind() == reflect.Ptr || val.Kind() == reflect.Interface {
        val = val.Elem()
    }
    var structFields []reflect.StructField

    for i := 0; i < val.NumField(); i++ {
        structFields = append(structFields, val.Type().Field(i))
    }

    for i := range args {
        if args[i] == nil {
            continue
        }
        args[i](&structFields)
    }

    var res StructInfo

    for _, field := range structFields {
        res.Fields = append(res.Fields, field.Tag.Get("db"))
    }

    return res
}
