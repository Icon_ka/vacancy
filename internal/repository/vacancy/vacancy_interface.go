package vacancy

import (
    "vacancy/internal/entites"
    "context"
)

type VacancyRepositorer interface {
    CreateIdentifier(context.Context, entites.Identifier) (int, error)
    CreateHiringOrganization(context.Context, entites.HiringOrganization) (int, error)
    CreateVacancy(context.Context, entites.Vacancy, int, int) error

    GetByIdIdentifier(context.Context, int) (entites.Identifier, error)
    GetByIdOrganization(context.Context, int) (entites.HiringOrganization, error)
    GetByIdVacancy(context.Context, int) (entites.Vacancy, error)
    GetCount(context.Context) (int, error)

    GetList(context.Context, ) ([]entites.Vacancy, error)

    Delete(context.Context, int, string) error
}
