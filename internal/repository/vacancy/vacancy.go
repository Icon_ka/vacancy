package vacancy

import (
    "github.com/jmoiron/sqlx"
    "vacancy/internal/entites"
    sq "github.com/Masterminds/squirrel"
    "context"
    "fmt"
)

type VacancyPostgers struct {
    pg         *sqlx.DB
    sqlBuilder sq.StatementBuilderType
}

func NewVacancyPostgres(db *sqlx.DB) *VacancyPostgers {
    builder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
    return &VacancyPostgers{pg: db, sqlBuilder: builder}
}

func (v VacancyPostgers) GetCount(context.Context) (int, error) {
    var count int
    err := v.pg.QueryRowContext(context.Background(), "SELECT COUNT(*) FROM vacancy").Scan(&count)
    if err != nil {
        return 0, err
    }

    return count, nil
}

func (v VacancyPostgers) CreateIdentifier(ctx context.Context, identifier entites.Identifier) (int, error) {
    sql, _, err := v.sqlBuilder.Insert("identifier").
        Columns("type", "name", "value").
        Values(identifier.Type, identifier.Name, identifier.Value).
        Suffix("RETURNING \"id\"").ToSql()

    if err != nil {
        return 0, err
    }

    var id int
    err = v.pg.QueryRowContext(ctx, sql, identifier.Type, identifier.Name, identifier.Value).Scan(&id)
    if err != nil {
        return 0, err
    }

    return id, nil
}

func (v VacancyPostgers) CreateHiringOrganization(ctx context.Context, organization entites.HiringOrganization) (int, error) {
    sql, _, err := v.sqlBuilder.Insert("hiring_organization").
        Columns("type", "name", "logo", "same_as").
        Values(organization.Type, organization.Name, organization.Logo, organization.SameAs).
        Suffix("RETURNING \"id\"").ToSql()

    if err != nil {
        return 0, err
    }

    var id int
    err = v.pg.QueryRowContext(ctx, sql, organization.Type, organization.Name, organization.Logo, organization.SameAs).Scan(&id)
    if err != nil {
        return 0, err
    }

    return id, nil
}

func (v VacancyPostgers) CreateVacancy(ctx context.Context, vacancy entites.Vacancy, identifierID int, organizationID int) error {
    sql, _, err := v.sqlBuilder.Insert("vacancy").
        Columns("context", "type", "date_posted", "title", "description", "identifier_id", "valid_through", "hiring_organization_id", "job_location", "job_location_type", "employment_type").
        Values(vacancy.Context, vacancy.Type, vacancy.DatePosted, vacancy.Title, vacancy.Description, identifierID, vacancy.ValidThrough, organizationID, vacancy.JobLocationType, vacancy.JobLocationType, vacancy.EmploymentType).
        Suffix("RETURNING \"id\"").ToSql()
    if err != nil {
        return err
    }

    _, err = v.pg.ExecContext(ctx, sql, vacancy.Context, vacancy.Type, vacancy.DatePosted, vacancy.Title, vacancy.Description, identifierID, vacancy.ValidThrough, organizationID, vacancy.JobLocationType, vacancy.JobLocationType, vacancy.EmploymentType)
    if err != nil {
        return err
    }

    return nil
}

func (v VacancyPostgers) GetByIdIdentifier(ctx context.Context, i int) (entites.Identifier, error) {
    sql, args, err := v.sqlBuilder.Select("*").
        From("identifier").
        Where(sq.Eq{"id": i}).ToSql()

    if err != nil {
        return entites.Identifier{}, err
    }

    var ident entites.Identifier
    err = v.pg.QueryRowxContext(ctx, sql, args...).StructScan(&ident)
    if err != nil {
        return entites.Identifier{}, err
    }
    fmt.Println("Identifier:", ident)

    return ident, nil
}

func (v VacancyPostgers) GetByIdOrganization(ctx context.Context, i int) (entites.HiringOrganization, error) {
    sql, args, err := v.sqlBuilder.Select("*").
        From("hiring_organization").
        Where(sq.Eq{"id": i}).ToSql()

    if err != nil {
        return entites.HiringOrganization{}, err
    }

    var ident entites.HiringOrganization
    err = v.pg.QueryRowxContext(ctx, sql, args...).StructScan(&ident)
    if err != nil {
        fmt.Println("Error:", err)
        return entites.HiringOrganization{}, err
    }

    return ident, nil
}

func (v VacancyPostgers) GetByIdVacancy(ctx context.Context, i int) (entites.Vacancy, error) {
    sql, args, err := v.sqlBuilder.Select("id", "context", "type", "date_posted", "title", "description", "valid_through", "job_location", "job_location_type").
        From("vacancy").
        Where(sq.Eq{"id": i}).ToSql()

    if err != nil {
        return entites.Vacancy{}, err
    }

    var ident entites.Vacancy
    err = v.pg.QueryRowxContext(ctx, sql, args...).StructScan(&ident)
    if err != nil {
        fmt.Println("Error:", err)
        return entites.Vacancy{}, err
    }

    return ident, nil
}

func (v VacancyPostgers) GetList(ctx context.Context) ([]entites.Vacancy, error) {
    sql, _, err := v.sqlBuilder.Select("*").
        From("vacancy").ToSql()

    if err != nil {
        return []entites.Vacancy{}, err
    }

    var ident []entites.Vacancy
    rows, err := v.pg.QueryContext(ctx, sql)
    if err != nil {
        return []entites.Vacancy{}, err
    }
    defer rows.Close()

    for rows.Next() {
        var vacancy entites.Vacancy
        err = rows.Scan(&vacancy.Id, &vacancy.Context, &vacancy.Type, &vacancy.DatePosted, &vacancy.Title, &vacancy.Description, &vacancy.Identifier.Id, &vacancy.ValidThrough, &vacancy.HiringOrganization.Id, &vacancy.JobLocation, &vacancy.JobLocationType, &vacancy.EmploymentType)
        if err != nil {
            return []entites.Vacancy{}, err
        }
        ident = append(ident, vacancy)
    }

    if err = rows.Err(); err != nil {
        return []entites.Vacancy{}, err
    }

    return ident, nil
}

func (v VacancyPostgers) Delete(ctx context.Context, i int, tableName string) error {
    sql, args, err := v.sqlBuilder.Delete(tableName).Where(sq.Eq{"id": i}).ToSql()
    if err != nil {
        return err
    }

    _, err = v.pg.ExecContext(ctx, sql, args...)
    if err != nil {
        return err
    }
    return nil
}
