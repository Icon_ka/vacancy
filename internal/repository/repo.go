package repository

import (
    "vacancy/internal/repository/vacancy"
    "github.com/jmoiron/sqlx"
)

type Repository struct {
    Vac vacancy.VacancyRepositorer
}

func NewRepo(db *sqlx.DB) *Repository {
    return &Repository{Vac: vacancy.NewVacancyPostgres(db)}
}
