package vacancyParser

import (
    "github.com/tebeka/selenium"
    "github.com/tebeka/selenium/chrome"
    "log"
    "os"
    "fmt"
)

const HabrLink = "http://career.habr.com"

type Vacancy struct {
    WD selenium.WebDriver
}

func NewVacancy() (Vacancer, error) {
    caps := selenium.Capabilities{
        "browserName": "chrome",
    }

    chrCaps := chrome.Capabilities{
        W3C: true,
    }
    caps.AddChrome(chrCaps)

    var wd selenium.WebDriver
    var err error
    i := 1

    for i < 5 {
        wd, err = selenium.NewRemote(caps, os.Getenv("URL"))
        if err != nil {
            log.Println(err)
            i++
            continue
        }
        break
    }

    return &Vacancy{
        WD: wd,
    }, nil
}

func (v Vacancy) GetCountRaw() (string, error) {
    elem, err := v.WD.FindElement(selenium.ByCSSSelector, ".search-total")
    if err != nil {
        return "", err
    }

    return elem.Text()
}

func (v Vacancy) GetLinks() ([]string, error) {
    elems, err := v.WD.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
    if err != nil {
        return nil, err
    }

    var links []string
    for i := range elems {
        var link string
        link, err = elems[i].GetAttribute("href")
        if err != nil {
            continue
        }

        links = append(links, HabrLink+link)
    }

    return links, nil
}

func (v Vacancy) ParsePage(page int, query string) error {
    return v.WD.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
}
