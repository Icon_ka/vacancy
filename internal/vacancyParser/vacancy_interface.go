package vacancyParser

type Vacancer interface {
    GetCountRaw() (string, error)
    GetLinks() ([]string, error)
    ParsePage(int, string) error
}
