package vacancyClient

import (
    "net/http"
    "fmt"
    "github.com/PuerkitoBio/goquery"
)

type VacancyClient struct {
    client *http.Client
}

func NewClient() *VacancyClient {
    return &VacancyClient{
        client: &http.Client{},
    }
}

func (c VacancyClient) GetPage(link string) string {

    resp, err := c.client.Get(link)
    if err != nil {
        fmt.Println(err)
        return ""
    }
    var doc *goquery.Document
    doc, err = goquery.NewDocumentFromReader(resp.Body)
    if err != nil && doc != nil {
        fmt.Println(err)
        return ""
    }

    dd := doc.Find("script[type=\"application/ld+json\"]")
    if dd == nil {
        fmt.Println("Habr no vacancy")
        return ""
    }

    return dd.First().Text()
}
