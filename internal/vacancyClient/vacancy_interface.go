package vacancyClient

type VacancyClienter interface {
    GetPage(link string) string
}
