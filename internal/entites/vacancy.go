package entites

// Vacancy model info
// @Description Вакансия.
type Vacancy struct {
    Id                 int                `db:"id" db_type:"SERIAL PRIMARY KEY"`
    Context            string             `json:"@context" db:"context" db_type:"VARCHAR(255)"`
    Type               string             `json:"@type" db:"type" db_type:"VARCHAR(255)"`
    DatePosted         string             `json:"datePosted" db:"date_posted" db_type:"VARCHAR(255)"`
    Title              string             `json:"title" db:"title" db_type:"VARCHAR(255)"`
    Description        string             `json:"description" db:"description" db_type:"TEXT"`
    Identifier         Identifier         `json:"identifier" db:"identifier_id" db_type:"INTEGER"`
    ValidThrough       string             `json:"validThrough" db:"valid_through" db_type:"VARCHAR(255)"`
    HiringOrganization HiringOrganization `json:"hiringOrganization" db:"hiring_organization_id" db_type:"INTEGER"`
    JobLocation        interface{}        `json:"jobLocation" db:"job_location" db_type:"TEXT"`
    JobLocationType    string             `json:"jobLocationType" db:"job_location_type" db_type:"VARCHAR(255)"`
    EmploymentType     string             `json:"employmentType" db:"employment_type" db_type:"VARCHAR(255)"`
}

func (v Vacancy) TableName() string {
    return "vacancy"
}

// HiringOrganization model info
// @Description Организация.
type HiringOrganization struct {
    Id     int    `db:"id" db_type:"SERIAL PRIMARY KEY"`
    Type   string `json:"@type" db:"type" db_type:"VARCHAR(255)"`
    Name   string `json:"name" db:"name" db_type:"VARCHAR(255)"`
    Logo   string `json:"logo" db:"logo" db_type:"VARCHAR(255)"`
    SameAs string `json:"sameAs" db:"same_as" db_type:"VARCHAR(255)"`
}

func (h HiringOrganization) TableName() string {
    return "hiring_organization"
}

// Identifier model info
// @Description Идентификатор.
type Identifier struct {
    Id    int    `db:"id" db_type:"SERIAL PRIMARY KEY"`
    Type  string `json:"@type" db:"type" db_type:"VARCHAR(255)"`
    Name  string `json:"name" db:"name" db_type:"VARCHAR(255)"`
    Value string `json:"value" db:"value" db_type:"VARCHAR(255)"`
}

func (i Identifier) TableName() string {
    return "identifier"
}
