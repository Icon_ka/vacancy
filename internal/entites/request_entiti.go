package entites

// Request model info
// @Description Запрос для поиска вакансий.
type Request struct {
    Query string `json:"query"`
}

// DeleteGetRequest model info
// @Description Запрос на удаление и поиск вакансий по ID.
type DeleteGetRequest struct {
    ID int `json:"id"`
}
