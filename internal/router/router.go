package router

import (
    "github.com/go-chi/chi"

    "net/http"
    "go.uber.org/zap"
    "vacancy/internal/controller"
    httpSwagger "github.com/swaggo/http-swagger"
    _ "vacancy/docs"
)

func NewApiRouter(controllers *controller.Controller) http.Handler {
    r := chi.NewRouter()

    r.Use(LoggerMiddleware)

    r.Get("/swagger/*", httpSwagger.Handler(
        httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
    ))

    r.Post("/search", controllers.Ctrl.Search)
    r.Post("/delete", controllers.Ctrl.Delete)
    r.Post("/get", controllers.Ctrl.Get)
    r.Get("/list", controllers.Ctrl.List)

    return r
}

func LoggerMiddleware(next http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        logger, _ := zap.NewProduction()
        defer logger.Sync()

        logger.Info("Request received",
            zap.String("path", r.URL.Path),
            zap.String("method", r.Method),
            zap.String("ip", r.RemoteAddr),
        )

        next.ServeHTTP(w, r)
    })
}
