package vacancy

import (
    "vacancy/internal/entites"
    "vacancy/internal/repository"
    "context"
    "vacancy/internal/vacancyClient"
    "vacancy/internal/vacancyParser"
    "github.com/jmoiron/sqlx"
    "encoding/json"
    "regexp"
    "strconv"
)

type VacancyService struct {
    Repo   repository.Repository
    Client vacancyClient.VacancyClient
    Parser vacancyParser.Vacancer
}

func NewVacancyService(db *sqlx.DB) *VacancyService {
    parser, err := vacancyParser.NewVacancy()
    if err != nil {
        return nil
    }

    return &VacancyService{
        Repo:   *repository.NewRepo(db),
        Client: *vacancyClient.NewClient(),
        Parser: parser,
    }
}

func (v VacancyService) Create(ctx context.Context, query string) ([]entites.Vacancy, error) {

    count, err := v.Repo.Vac.GetCount(ctx)
    if err != nil {
        return []entites.Vacancy{}, err
    }

    if count == 0 {
        err = v.Parser.ParsePage(1, query)
        if err != nil {
            return []entites.Vacancy{}, err
        }

        str, err := v.Parser.GetCountRaw()
        if err != nil {
            return []entites.Vacancy{}, err
        }

        re := regexp.MustCompile(`\d+`)

        matches := re.FindAllString(str, -1)

        num, err := strconv.Atoi(matches[0])
        if err != nil {
            return []entites.Vacancy{}, err
        }

        num /= 25

        for i := 1; i <= num+1; i++ {
            if i > 1 {
                err := v.Parser.ParsePage(i, query)
                if err != nil {
                    return []entites.Vacancy{}, err
                }
            }
            links, err := v.Parser.GetLinks()
            if err != nil {
                return []entites.Vacancy{}, err
            }

            for _, link := range links {
                var vacancy entites.Vacancy
                page := v.Client.GetPage(link)

                err := json.Unmarshal([]byte(page), &vacancy)
                if err != nil {
                    return []entites.Vacancy{}, err
                }

                identID, err := v.Repo.Vac.CreateIdentifier(ctx, vacancy.Identifier)
                if err != nil {
                    return []entites.Vacancy{}, err
                }

                orgID, err := v.Repo.Vac.CreateHiringOrganization(ctx, vacancy.HiringOrganization)

                err = v.Repo.Vac.CreateVacancy(ctx, vacancy, identID, orgID)
                if err != nil {
                    return []entites.Vacancy{}, err
                }
            }
        }
    }

    return v.List(ctx)
}

func (v VacancyService) GetByID(ctx context.Context, i int) (entites.Vacancy, error) {
    ident, err := v.Repo.Vac.GetByIdIdentifier(ctx, i)
    if err != nil {
        return entites.Vacancy{}, nil
    }

    org, err := v.Repo.Vac.GetByIdOrganization(ctx, i)
    if err != nil {
        return entites.Vacancy{}, nil
    }

    var vacancy entites.Vacancy

    vacancy, err = v.Repo.Vac.GetByIdVacancy(ctx, i)
    if err != nil {
        return entites.Vacancy{}, nil
    }

    vacancy.Identifier = ident
    vacancy.HiringOrganization = org

    return vacancy, nil
}

func (v VacancyService) List(ctx context.Context) ([]entites.Vacancy, error) {
    return v.Repo.Vac.GetList(ctx)
}

func (v VacancyService) Delete(ctx context.Context, i int) error {
    err := v.Repo.Vac.Delete(ctx, i, "vacancy")
    if err != nil {
        return err
    }
    err = v.Repo.Vac.Delete(ctx, i, "identifier")
    if err != nil {
        return err
    }
    err = v.Repo.Vac.Delete(ctx, i, "hiring_organization")
    if err != nil {
        return err
    }

    return nil
}
