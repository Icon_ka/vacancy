package vacancy

import (
    "vacancy/internal/entites"
    "context"
)

type VacancyServicer interface {
    Create(context.Context, string) ([]entites.Vacancy, error)
    GetByID(context.Context, int) (entites.Vacancy, error)
    List(context.Context) ([]entites.Vacancy, error)
    Delete(context.Context, int) error
}
