package service

import (
    "vacancy/internal/service/vacancy"
    "github.com/jmoiron/sqlx"
)

type Service struct {
    Vac vacancy.VacancyServicer
}

func NewService(db *sqlx.DB) *Service {
    return &Service{Vac: vacancy.NewVacancyService(db)}
}
